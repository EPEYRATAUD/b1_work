
# Verrouille le PC après 30 secondes
$time = $args[1]
$command = $args[0]

if($command -eq "lock"){
    Start-Sleep $time
    rundll32.exe user32.dll,LockWorkStation
}

#Eteins le PC après 75 secondes 
$time = $args[1]
$command = $args[0]

if($command -eq "shutdown"){
    Start-Sleep $time
    shutdown /s
}
