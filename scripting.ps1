#Enzo PEYRATAUD
#19/10/2020
# Script tp 1 Win Client : consiste pour chaque objectif à executer une ligne de commande pour récupérer une information système de la machine

Write-Host "Nom de la machine : " -ForegroundColor Green
Hostname 

Write-Host "IP principale : " -ForegroundColor Green
((ipconfig | findstr "IPv4"))

Write-Host " OS et version de l'OS : " -ForegroundColor Green    
(Get-WmiObject -class Win32_OperatingSystem).Caption 
(Get-WmiObject -class Win32_OperatingSystem).Version

Write-Host "Date et heure d'allumage : " -ForegroundColor Green
((net statistics workstation | findstr "depuis"))

Write-Host "Mise a jour de l'OS : "  -ForegroundColor Green
(Get-CimInstance -ClassName CIM_OperatingSystem).Version -ge [Version]


Write-Host "Taille Ram utilise / disponible : " -ForegroundColor Green
$RAMinfo = Get-CIMInstance Win32_OperatingSystem | Select-Object FreePhysicalMemory,TotalVisibleMemorySize
$cs = get-wmiobject -class "Win32_ComputerSystem"
$Mem = [math]::Ceiling($cs.TotalPhysicalMemory / 1024 / 1024 / 1024)
$ramfree = $RAMinfo.FreePhysicalMemory * 1.0E-6
$UsedRam = $Mem - $ramfree
$URam = ' - Used : ' + $UsedRam + ' Gi'
$FRam = ' - Free : ' + $ramfree + ' Gi'
Write-Output $URam, $FRam



Write-Host "Taille stockage utilise / disponible : " -ForegroundColor Green 
$diskSpace = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Measure-Object Size -Sum).sum /1gb)
$diskFree = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Measure-Object FreeSpace -Sum).sum /1gb)
$diskUsed = [math]::Round($diskSpace-$diskFree)
$UDisk = ' - Used : ' + $diskUsed + ' G'
$FDisk = ' - Free : ' + $diskFree + ' G'
Write-Output $UDisk, $FDisk

Write-Host " temps reponse moyen vers 8.8.8.8 :" -ForegroundColor Green
ping 8.8.8.8
 

Write-Host " liste utilisateurs de la machine : " -ForegroundColor Green
((Get-LocalUser | Select-Object Name))

