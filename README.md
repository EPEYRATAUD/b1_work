# b1_work

# Maîtrise de poste - Day 1


# I. Self-footprinting

## 1. Host OS

```

PS C:\Users\lucas\TP\project-1> hostname                                              
LAPTOP-JHK9K6A7


C:\Users\lucas>ver

Microsoft Windows [version 10.0.18363.1082]


C:\Users\lucas\TP\project-1> systeminfo 

[...]
Type du système: x64-based PC
[...]


C:\Users\lucas>wmic MemoryChip get BankLabel, Capacity, MemoryType, TypeDetail, Speed

BankLabel  Capacity    MemoryType  Speed  TypeDetail
BANK 0     8589934592  0           2667   128
BANK 2     8589934592  0           2667   128


C:\Users\lucas\TP\project-1> Get-CimInstance win32_physicalmemory

[...]
Manufacturer : SK Hynix
[...]

```

## 2. Devices

```

C:\Users\lucas\TP\project-1> wmic cpu get

[...]
Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz
[...]

C:\Users\lucas\TP\project-1>WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors

DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      4              8

Intel Core i5 -> 4 coeur natif , correspond au milieu de gamme des cpu Intel 

9300H -> 9ème génération / 300 -> numéro SKU /  H signifie processeur hautes performances 

C:\Users\lucas>wmic path win32_VideoController get name
, 
Name
NVIDIA GeForce RTX 2060



C:\Users\lucas\TP\project-1> Get-PhysicalDisk

 [...]
 SAMSUNG MZVLB512HBJQ-000H1
 [...]
 
 [...]
 SSD
 [...]
 
 
 C:\Users\lucas>diskpart
 
 DISKPART> sel disk 0
 
 DISKPART> detail disk
 
  [...]
  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   Windows      NTFS   Partition    476 G   Sain       Démarrag
  Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Système
  [...]
  
  ```
  
  ## 3. Users
  
  ```
  
  C:\Users\lucas> wmic useraccount list full
  
  AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=LAPTOP-JHK9K6A7
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2594154554-3504087977-2210613450-500
SIDType=1
Status=Degraded


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=LAPTOP-JHK9K6A7
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2594154554-3504087977-2210613450-503
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=LAPTOP-JHK9K6A7
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2594154554-3504087977-2210613450-501
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=LAPTOP-JHK9K6A7
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=lucas
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2594154554-3504087977-2210613450-1001
SIDType=1
Status=OK


AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=LAPTOP-JHK9K6A7
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-2594154554-3504087977-2210613450-504
SIDType=1
Status=Degraded

```

## 4. Processus

```
C:\Users\lucas>tasklist

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     2 600 Ko
Registry                       120 Services                   0    99 548 Ko
smss.exe                       492 Services                   0     1 204 Ko
csrss.exe                      732 Services                   0     5 468 Ko
wininit.exe                    824 Services                   0     6 896 Ko
csrss.exe                      832 Console                    1     5 456 Ko
services.exe                   896 Services                   0    10 720 Ko
winlogon.exe                   928 Console                    1    12 240 Ko
lsass.exe                      956 Services                   0    20 884 Ko
svchost.exe                    736 Services                   0     3 900 Ko
svchost.exe                    516 Services                   0    31 688 Ko
fontdrvhost.exe                560 Console                    1     4 548 Ko
fontdrvhost.exe               1032 Services                   0     3 620 Ko
WUDFHost.exe                  1076 Services                   0    10 584 Ko
svchost.exe                   1172 Services                   0    16 016 Ko
svchost.exe                   1224 Services                   0     8 672 Ko
dwm.exe                       1308 Console                    1    61 312 Ko
svchost.exe                   1364 Services                   0    10 240 Ko
svchost.exe                   1396 Services                   0     8 976 Ko
svchost.exe                   1404 Services                   0    12 672 Ko
svchost.exe                   1412 Services                   0    11 680 Ko
svchost.exe                   1532 Services                   0     8 816 Ko
svchost.exe                   1564 Services                   0    10 812 Ko
svchost.exe                   1572 Services                   0    12 228 Ko
svchost.exe                   1692 Services                   0    15 880 Ko
svchost.exe                   1808 Services                   0     5 972 Ko
svchost.exe                   1836 Services                   0     8 688 Ko
svchost.exe                   1864 Services                   0    15 320 Ko
svchost.exe                   1872 Services                   0     9 556 Ko
svchost.exe                   1956 Services                   0     6 024 Ko
svchost.exe                   1220 Services                   0    20 648 Ko
svchost.exe                   2116 Services                   0    10 128 Ko
svchost.exe                   2248 Services                   0     7 780 Ko
svchost.exe                   2280 Services                   0     7 000 Ko
svchost.exe                   2288 Services                   0     7 176 Ko
AppHelperCap.exe              2424 Services                   0    17 936 Ko
SysInfoCap.exe                2436 Services                   0    17 752 Ko
OmenCap.exe                   2448 Services                   0    14 136 Ko
NetworkCap.exe                2456 Services                   0    11 104 Ko
TouchpointAnalyticsClient     2464 Services                   0    79 400 Ko
svchost.exe                   2560 Services                   0    17 224 Ko
svchost.exe                   2572 Services                   0    11 060 Ko
svchost.exe                   2580 Services                   0    12 984 Ko
dasHost.exe                   2616 Services                   0    11 824 Ko
svchost.exe                   2792 Services                   0    10 988 Ko
svchost.exe                   2864 Services                   0     8 796 Ko
svchost.exe                   2992 Services                   0    36 372 Ko
svchost.exe                   3028 Services                   0     7 716 Ko
SynTPEnhService.exe           3088 Services                   0    10 044 Ko
WmiPrvSE.exe                  3132 Services                   0    34 888 Ko
NVDisplay.Container.exe       3168 Services                   0    18 556 Ko
unsecapp.exe                  3296 Services                   0     6 884 Ko
WmiPrvSE.exe                  3412 Services                   0     9 952 Ko
svchost.exe                   3492 Services                   0     5 848 Ko
svchost.exe                   3500 Services                   0    13 288 Ko
svchost.exe                   3508 Services                   0     8 416 Ko
svchost.exe                   3568 Services                   0     8 740 Ko
svchost.exe                   3576 Services                   0     8 380 Ko
Memory Compression            3588 Services                   0     5 004 Ko
NVDisplay.Container.exe       3732 Console                    1    45 004 Ko
svchost.exe                   3892 Services                   0     7 572 Ko
svchost.exe                   4020 Services                   0    14 764 Ko
audiodg.exe                   3656 Services                   0    25 176 Ko
svchost.exe                   4176 Services                   0     6 896 Ko
svchost.exe                   4184 Services                   0    10 080 Ko
svchost.exe                   4312 Services                   0    20 548 Ko
svchost.exe                   4364 Services                   0     8 804 Ko
svchost.exe                   4452 Services                   0    12 548 Ko
spoolsv.exe                   4576 Services                   0    24 820 Ko
svchost.exe                   4664 Services                   0    19 632 Ko
wlanext.exe                   4700 Services                   0     5 656 Ko
conhost.exe                   4724 Services                   0    10 540 Ko
svchost.exe                   4744 Services                   0     8 248 Ko
agent_ovpnconnect_1598032     4960 Services                   0     7 692 Ko
DbxSvc.exe                    4968 Services                   0     6 620 Ko
DTSAPO3Service.exe            4992 Services                   0    12 432 Ko
svchost.exe                   5008 Services                   0    22 772 Ko
OfficeClickToRun.exe          5024 Services                   0    57 724 Ko
svchost.exe                   5040 Services                   0     8 016 Ko
svchost.exe                   5052 Services                   0    26 696 Ko
ibtsiva.exe                   5064 Services                   0     4 736 Ko
svchost.exe                   5096 Services                   0    12 596 Ko
pmservice.exe                 5140 Services                   0     8 500 Ko
nvcontainer.exe               5168 Services                   0    37 232 Ko
svchost.exe                   5176 Services                   0     6 652 Ko
RtkAudUService64.exe          5184 Services                   0     9 952 Ko
RstMwService.exe              5196 Services                   0     7 100 Ko
sqlwriter.exe                 5228 Services                   0     7 916 Ko
svchost.exe                   5236 Services                   0     5 444 Ko
svchost.exe                   5244 Services                   0    13 376 Ko
svchost.exe                   5252 Services                   0     7 400 Ko
SynAudSrv.exe                 5344 Services                   0     8 376 Ko
svchost.exe                   5460 Services                   0     5 748 Ko
CxAudioSvc.exe                5496 Services                   0    28 516 Ko
MsMpEng.exe                   5520 Services                   0   159 840 Ko
svchost.exe                   5540 Services                   0    21 280 Ko
jhi_service.exe               5888 Services                   0     6 208 Ko
svchost.exe                   6052 Services                   0    12 248 Ko
svchost.exe                   6356 Services                   0     7 140 Ko
sihost.exe                    6540 Console                    1    32 184 Ko
svchost.exe                   6568 Console                    1    31 332 Ko
svchost.exe                   6576 Console                    1     8 324 Ko
svchost.exe                   6644 Console                    1    48 224 Ko
DropboxUpdate.exe             6728 Services                   0     3 836 Ko
taskhostw.exe                 6756 Console                    1    21 944 Ko
svchost.exe                   6796 Services                   0    20 748 Ko
svchost.exe                   6884 Services                   0     7 844 Ko
ctfmon.exe                    6928 Console                    1    17 536 Ko
rundll32.exe                  7024 Console                    1     7 396 Ko
explorer.exe                  7364 Console                    1   120 968 Ko
svchost.exe                   7676 Services                   0    19 916 Ko
svchost.exe                   7748 Services                   0     5 780 Ko
svchost.exe                   7952 Console                    1    27 144 Ko
svchost.exe                   7976 Services                   0     7 732 Ko
svchost.exe                   8052 Services                   0     9 764 Ko
svchost.exe                   8124 Console                    1    27 372 Ko
nvcontainer.exe               7472 Console                    1    27 072 Ko
nvcontainer.exe               6768 Console                    1    46 952 Ko
svchost.exe                   8152 Services                   0    12 544 Ko
svchost.exe                   8516 Services                   0    17 148 Ko
pmropn.exe                    8600 Console                    1   107 544 Ko
StartMenuExperienceHost.e     8760 Console                    1    81 136 Ko
SearchIndexer.exe             8840 Services                   0    39 484 Ko
svchost.exe                   8996 Services                   0     9 012 Ko
RuntimeBroker.exe             9108 Console                    1    24 820 Ko
SynTPEnh.exe                   952 Console                    1    22 240 Ko
svchost.exe                   5204 Services                   0     5 496 Ko
NisSrv.exe                    9488 Services                   0    12 796 Ko
SearchUI.exe                  9700 Console                    1   273 780 Ko
unsecapp.exe                 10100 Console                    1     9 340 Ko
RuntimeBroker.exe            10196 Console                    1    43 772 Ko
YourPhone.exe                10364 Console                    1     3 276 Ko
SettingSyncHost.exe          10472 Console                    1     9 016 Ko
TouchpointGpuInfo.exe        10536 Services                   0    47 672 Ko
LockApp.exe                  11040 Console                    1    54 828 Ko
cmd.exe                       6808 Console                    1    12 440 Ko
cmd.exe                       4980 Console                    1    12 424 Ko
RuntimeBroker.exe            11188 Console                    1    34 060 Ko
pmropn32.exe                 11924 Console                    1     5 844 Ko
svchost.exe                  11932 Services                   0     8 972 Ko
pmropn64.exe                 11948 Console                    1     5 332 Ko
RuntimeBroker.exe            12024 Console                    1    29 040 Ko
NVIDIA Web Helper.exe        11696 Console                    1    15 868 Ko
conhost.exe                  12260 Console                    1     1 012 Ko
OmenCommandCenterBackgrou    12284 Console                    1   181 676 Ko
dllhost.exe                  12228 Services                   0    10 332 Ko
HPSystemEventUtilityHost.    12292 Console                    1    73 080 Ko
nvsphelper64.exe             12580 Console                    1    15 220 Ko
NVIDIA Share.exe             12588 Console                    1    53 908 Ko
NVIDIA Share.exe             12812 Console                    1    35 832 Ko
RuntimeBroker.exe              632 Console                    1    21 668 Ko
SecurityHealthSystray.exe     9068 Console                    1    10 808 Ko
SecurityHealthService.exe    12480 Services                   0    13 936 Ko
WmiPrvSE.exe                 13324 Services                   0     9 896 Ko
NVIDIA Share.exe             13412 Console                    1    71 304 Ko
RtkAudUService64.exe         13512 Console                    1    12 320 Ko
DTAgent.exe                  13620 Console                    1    68 412 Ko
DiscSoftBusServiceLite.ex    13708 Services                   0    14 488 Ko
msedge.exe                   13828 Console                    1   269 044 Ko
msedge.exe                   13856 Console                    1    10 520 Ko
msedge.exe                   14060 Console                    1   177 680 Ko
msedge.exe                   14068 Console                    1   119 376 Ko
msedge.exe                   14244 Console                    1    44 504 Ko
msedge.exe                   14252 Console                    1    68 024 Ko
msedge.exe                   14316 Console                    1    61 464 Ko
msedge.exe                   13704 Console                    1   292 168 Ko
msedge.exe                   14456 Console                    1    51 684 Ko
msedge.exe                   14468 Console                    1    63 736 Ko
msedge.exe                   14704 Console                    1    82 024 Ko
msedge.exe                   14712 Console                    1    45 140 Ko
msedge.exe                   14760 Console                    1    37 648 Ko
msedge.exe                   15128 Console                    1    96 240 Ko
msedge.exe                   15136 Console                    1    98 784 Ko
msedge.exe                   15144 Console                    1   109 076 Ko
omen.exe                      8136 Console                    1    13 976 Ko
msedge.exe                   15352 Console                    1    50 480 Ko
msedge.exe                    1072 Console                    1    46 996 Ko
msedge.exe                    3952 Console                    1    45 820 Ko
msedge.exe                    3232 Console                    1    97 108 Ko
msedge.exe                    3524 Console                    1    37 208 Ko
msedge.exe                   15440 Console                    1    20 476 Ko
msedge.exe                   16184 Console                    1    57 812 Ko
msedge.exe                   15484 Console                    1    43 628 Ko
msedge.exe                   16252 Console                    1    37 496 Ko
svchost.exe                  15800 Services                   0     7 524 Ko
msedge.exe                    2160 Console                    1    63 516 Ko
msedge.exe                   15904 Console                    1    98 392 Ko
msedge.exe                   15724 Console                    1    91 872 Ko
msedge.exe                   15752 Console                    1    47 032 Ko
msedge.exe                   15928 Console                    1    44 124 Ko
msedge.exe                   15696 Console                    1    85 056 Ko
svchost.exe                  16532 Services                   0    13 576 Ko
svchost.exe                  16592 Services                   0    45 324 Ko
svchost.exe                  16644 Services                   0    16 416 Ko
svchost.exe                  16792 Services                   0    11 876 Ko
svchost.exe                  17384 Services                   0    19 636 Ko
HPAudioSwitch.exe            17828 Console                    1    49 796 Ko
HPCommRecovery.exe           15984 Services                   0    24 400 Ko
SgrmBroker.exe               18132 Services                   0     6 060 Ko
svchost.exe                    524 Services                   0    14 352 Ko
svchost.exe                  11592 Services                   0     9 712 Ko
msedge.exe                   15240 Console                    1    95 508 Ko
dllhost.exe                  18028 Console                    1    15 112 Ko
msedge.exe                    2640 Console                    1    78 300 Ko
msedge.exe                   11424 Console                    1    42 032 Ko
msedge.exe                    9156 Console                    1    69 456 Ko
msedge.exe                   17488 Console                    1    63 700 Ko
msedge.exe                    6660 Console                    1    23 892 Ko
ApplicationFrameHost.exe     10996 Console                    1    38 884 Ko
WinStore.App.exe             10868 Console                    1   126 388 Ko
RuntimeBroker.exe             1352 Console                    1    27 624 Ko
Music.UI.exe                  8368 Console                    1    42 436 Ko
svchost.exe                   5588 Services                   0    12 280 Ko
SystemSettings.exe           10300 Console                    1    59 308 Ko
svchost.exe                   5364 Services                   0    12 084 Ko
smartscreen.exe               9244 Console                    1    24 884 Ko
Discord.exe                   7276 Console                    1    66 648 Ko
Discord.exe                  16932 Console                    1   107 588 Ko
Discord.exe                  16820 Console                    1    27 012 Ko
Discord.exe                   8924 Console                    1    15 164 Ko
Discord.exe                   3488 Console                    1   233 932 Ko
Discord.exe                  11828 Console                    1    19 776 Ko
ShellExperienceHost.exe      16808 Console                    1    60 276 Ko
RuntimeBroker.exe             1948 Console                    1    20 780 Ko
cmd.exe                       4304 Console                    1     4 008 Ko
conhost.exe                   4464 Console                    1    20 504 Ko
backgroundTaskHost.exe       16904 Console                    1    23 476 Ko
powershell.exe               11844 Console                    1    76 352 Ko
conhost.exe                  11512 Console                    1    17 316 Ko
cmd.exe                      11864 Console                    1     4 008 Ko
tasklist.exe                 11460 Console                    1     8 508 Ko



```

### csrss.exe 

* gère les applications consoles, la création et la destruction de threads et quelques parties de l'environnement 16 bits virtuel MS-DOS

### dwm.exe 

* gère les effets graphiques avancés.

### services.exe 

* Il s'agit du Service Control Manager (gestionnaire de contrôle des services) , responsable du démarrage, de l'arrêt et de l'interaction avec les services système .

### svchost.exe 

* Fonctionne en tant qu'hôte pour d'autres processus tournant à partir de Dlls, ces processus fonctionnent en tant que services. Un processus svchost.exe peut gérer plusieurs autres processus. 

### lsass.exe 

* s'agit du serveur local d'authentification de sécurité, il génère le processus responsable de l'authentification des utilisateurs . Ce processus peut être ouvert par plusieurs types de services dont voici la liste :
* Emplacement protégé
* Fournisseur de la prise en charge de sécurité LM NT
* Gestionnaire de compte de sécurité
* Ouverture de session réseau
* Service IPSEC







## 5. Network

```


C:\Users\lucas> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Ethernet 2                Realtek USB GbE Family Controller            20 Up           00-E0-4C-68-29-DF       100 Mbps
Connexion au réseau local TAP-Windows Adapter V9 for OpenVPN C...      19 Disconnected 00-FF-C2-DD-14-E9         1 Gbps
Ethernet                  Realtek Gaming GbE Family Controller         15 Disconnected 04-0E-3C-A3-D6-DD          0 bps
Wi-Fi                     Intel(R) Wireless-AC 9560 160MHz              6 Disconnected 80-32-53-E2-78-04          0 bps

````

### Ethernet / Ethernet 2 

* connexion filaire via le port RJ45 / adaptateur , passant après par un intermédiaire ( CPL , Hub , prise murale , Switch)


### Connexion au réseau local 

* connexion en réseau prové local a partir d'une application ( OpenVPN)


### Wi-Fi 

* permet de connecter sa machine sans passe par un réseau filaire , fonctionne par un réseau d'ondes électromagnétiques.
 
```




C:\Users\lucas> netstat -a

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:445            LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:5040           LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:5357           LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:45769          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:49664          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:49665          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:49666          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:49667          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:49669          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    0.0.0.0:49684          LAPTOP-JHK9K6A7:0      LISTENING
  TCP    127.0.0.1:6463         LAPTOP-JHK9K6A7:0      LISTENING
  TCP    127.0.0.1:8254         LAPTOP-JHK9K6A7:0      LISTENING
  TCP    127.0.0.1:49735        LAPTOP-JHK9K6A7:65001  ESTABLISHED
  TCP    127.0.0.1:49738        LAPTOP-JHK9K6A7:0      LISTENING
  TCP    127.0.0.1:49738        LAPTOP-JHK9K6A7:49756  ESTABLISHED
  TCP    127.0.0.1:49756        LAPTOP-JHK9K6A7:49738  ESTABLISHED
  TCP    127.0.0.1:65001        LAPTOP-JHK9K6A7:0      LISTENING
  TCP    127.0.0.1:65001        LAPTOP-JHK9K6A7:49735  ESTABLISHED
  TCP    192.168.1.32:139       LAPTOP-JHK9K6A7:0      LISTENING
  TCP    192.168.1.32:49691     40.67.254.36:https     ESTABLISHED
  TCP    192.168.1.32:49898     ec2-35-174-127-31:https  ESTABLISHED
  TCP    192.168.1.32:49911     ec2-54-209-238-49:https  ESTABLISHED
  TCP    192.168.1.32:49922     40.67.254.36:https     ESTABLISHED
  TCP    192.168.1.32:49976     ec2-54-184-132-124:8883  ESTABLISHED
  TCP    192.168.1.32:51378     ec2-18-182-129-64:https  ESTABLISHED
  TCP    192.168.1.32:51883     android:8009           ESTABLISHED
  TCP    192.168.1.32:52220     162.159.134.234:https  ESTABLISHED
  TCP    192.168.1.32:52491     162.159.134.233:https  ESTABLISHED
  TCP    192.168.1.32:52521     fra16s12-in-f3:https   ESTABLISHED
  TCP    192.168.1.32:52525     151.101.122.114:https  ESTABLISHED
  TCP    192.168.1.32:52534     151.101.122.114:https  ESTABLISHED
  TCP    192.168.1.32:52584     162.159.135.232:https  ESTABLISHED
  TCP    192.168.1.32:52589     1:https                ESTABLISHED
  TCP    192.168.1.32:52590     162.159.130.232:https  ESTABLISHED
  TCP    192.168.1.32:52605     a88-221-113-18:https   TIME_WAIT
  TCP    192.168.1.32:52606     a-0001:https           ESTABLISHED
  TCP    192.168.1.32:52608     bingforbusiness:https  ESTABLISHED
  TCP    192.168.1.32:52612     a23-44-193-216:https   ESTABLISHED
  TCP    192.168.1.32:52615     par10s39-in-f10:https  TIME_WAIT
  TCP    192.168.1.32:52618     server-13-249-33-198:https  TIME_WAIT
  TCP    192.168.1.32:52620     a104-126-233-250:https  ESTABLISHED
  TCP    192.168.1.32:52621     a2-20-88-229:https     TIME_WAIT
  TCP    192.168.1.32:52622     a104-126-233-250:https  ESTABLISHED
  TCP    192.168.1.32:52623     104.19.183.2:https     ESTABLISHED
  TCP    192.168.1.32:52624     server-13-249-11-95:https  ESTABLISHED
  TCP    192.168.1.32:52625     server-13-32-141-123:https  ESTABLISHED
  TCP    192.168.1.32:52628     a23-43-212-63:https    ESTABLISHED
  TCP    192.168.1.32:52630     server-13-249-33-198:https  ESTABLISHED
  TCP    192.168.1.32:52631     151.101.122.133:https  ESTABLISHED
  TCP    192.168.1.32:52632     a2-23-15-98:https      ESTABLISHED
  TCP    192.168.1.32:52633     a23-40-113-126:https   ESTABLISHED
  TCP    192.168.1.32:52636     a104-126-227-176:https  ESTABLISHED
  TCP    192.168.1.32:52643     ad9411418cf2cdacd:https  ESTABLISHED
  TCP    192.168.1.32:52644     server-13-249-11-46:https  ESTABLISHED
  TCP    192.168.1.32:52645     ad9411418cf2cdacd:https  TIME_WAIT
  TCP    192.168.1.32:52646     104.19.183.2:https     ESTABLISHED
  TCP    192.168.1.32:52649     ec2-35-156-107-113:https  ESTABLISHED
  TCP    192.168.1.32:52651     104-153-197-189:https  ESTABLISHED
  TCP    192.168.1.32:52653     a23-40-113-94:https    TIME_WAIT
  TCP    192.168.1.32:52661     bidder:https           TIME_WAIT
  TCP    192.168.1.32:52664     49:https               ESTABLISHED
  TCP    192.168.1.32:52666     151.101.122.132:https  ESTABLISHED
  TCP    192.168.1.32:52667     server-13-249-11-65:https  TIME_WAIT
  TCP    192.168.1.32:52671     server-13-249-11-65:https  ESTABLISHED
  TCP    192.168.1.32:52672     151.101.122.133:https  ESTABLISHED
  TCP    192.168.1.32:52673     fra15s10-in-f68:https  ESTABLISHED
  TCP    192.168.1.32:52674     49:https               TIME_WAIT
  TCP    192.168.1.32:52676     49:https               ESTABLISHED
  TCP    192.168.1.32:52679     151.101.120.84:https   ESTABLISHED
  TCP    192.168.1.32:52682     a23-40-112-237:https   ESTABLISHED
  TCP    192.168.1.32:52687     a23-40-113-157:https   ESTABLISHED
  TCP    192.168.1.32:52688     a23-40-113-157:https   ESTABLISHED
  TCP    192.168.1.32:52692     server-13-249-11-65:https  ESTABLISHED
  TCP    192.168.1.32:52693     152.199.22.243:https   ESTABLISHED
  TCP    192.168.1.32:52694     a23-40-113-27:https    ESTABLISHED
  TCP    192.168.1.32:52695     a23-40-113-27:https    ESTABLISHED
  TCP    192.168.1.32:52699     151.101.121.108:https  ESTABLISHED
  TCP    192.168.1.32:52702     151.101.122.217:https  ESTABLISHED
  TCP    192.168.1.32:52703     104.18.12.165:https    ESTABLISHED
  TCP    192.168.1.32:52706     175:https              ESTABLISHED
  TCP    192.168.1.32:52716     ec2-3-124-251-221:https  TIME_WAIT
  TCP    192.168.1.32:52722     175:https              TIME_WAIT
  TCP    192.168.1.32:52729     104.20.10.37:https     ESTABLISHED
  TCP    192.168.1.32:52733     data53:https           TIME_WAIT
  TCP    192.168.1.32:52734     par21s04-in-f1:https   TIME_WAIT
  TCP    192.168.1.32:52735     par21s04-in-f1:https   ESTABLISHED
  TCP    192.168.1.32:52736     ec2-52-86-108-169:https  TIME_WAIT
  TCP    192.168.1.32:52737     par21s04-in-f1:https   TIME_WAIT
  TCP    192.168.1.32:52738     par21s04-in-f1:https   ESTABLISHED
  TCP    192.168.1.32:52739     ec2-34-247-250-37:https  ESTABLISHED
  TCP    192.168.1.32:52740     a23-40-113-126:https   TIME_WAIT
  TCP    192.168.1.32:52743     104.45.23.134:https    ESTABLISHED
  TCP    192.168.1.32:52744     a104-126-233-250:https  ESTABLISHED
  TCP    192.168.1.32:52757     a23-212-157-235:https  ESTABLISHED
  TCP    192.168.1.32:52758     a23-212-157-235:https  ESTABLISHED
  TCP    192.168.1.32:52759     ec2-34-247-250-37:https  ESTABLISHED
  TCP    192.168.1.32:52760     ec2-34-247-250-37:https  ESTABLISHED
  TCP    192.168.1.32:52761     server-99-86-90-98:https  ESTABLISHED
  TCP    192.168.1.32:52762     104.19.169.96:https    ESTABLISHED
  TCP    192.168.1.32:52764     178.250.6.177:https    TIME_WAIT
  TCP    192.168.1.32:52766     178.250.0.157:https    TIME_WAIT
  TCP    192.168.1.32:52768     52.142.114.2:https     ESTABLISHED
  TCP    192.168.1.32:52769     52.114.74.45:https     ESTABLISHED
  TCP    192.168.1.32:52770     a-0003:https           ESTABLISHED
  TCP    192.168.1.32:52772     52.111.231.0:https     ESTABLISHED
  TCP    192.168.1.32:52778     a2-20-88-229:https     TIME_WAIT
  TCP    192.168.1.32:52795     a23-45-115-75:https    TIME_WAIT
  TCP    192.168.1.32:52814     151.101.121.108:https  ESTABLISHED
  TCP    192.168.1.32:52818     par10s28-in-f97:https  ESTABLISHED
  TCP    192.168.1.32:52819     server-99-86-90-106:https  ESTABLISHED
  TCP    192.168.1.32:52821     a23-212-157-94:https   TIME_WAIT
  TCP    192.168.1.32:52829     a23-212-157-94:https   TIME_WAIT
  TCP    192.168.1.32:52838     23.111.8.18:https      TIME_WAIT
  TCP    192.168.1.32:52840     46:https               ESTABLISHED
  TCP    192.168.1.32:52842     151.101.122.49:https   ESTABLISHED
  TCP    192.168.1.32:52843     a23-40-112-237:https   ESTABLISHED
  TCP    192.168.1.32:52844     a23-40-112-237:https   ESTABLISHED
  TCP    192.168.1.32:52846     185.29.135.181:https   ESTABLISHED
  TCP    192.168.1.32:52848     server-99-86-90-69:https  ESTABLISHED
  TCP    192.168.1.32:52851     173.194.190.137:https  TIME_WAIT
  TCP    192.168.1.32:52854     a23-40-113-206:https   TIME_WAIT
  TCP    192.168.1.32:52855     176.74.173.248:https   ESTABLISHED
  TCP    192.168.1.32:52859     ec2-52-16-26-2:https   ESTABLISHED
  TCP    192.168.1.32:52867     178.250.0.157:https    TIME_WAIT
  TCP    192.168.1.32:52868     ec2-52-54-178-155:https  ESTABLISHED
  TCP    192.168.1.32:52869     ec2-52-54-178-155:https  ESTABLISHED
  TCP    192.168.1.32:52870     ec2-3-248-156-219:https  ESTABLISHED
  TCP    192.168.1.32:52871     ec2-52-30-144-189:https  ESTABLISHED
  TCP    192.168.1.32:52874     148:https              ESTABLISHED
  TCP    192.168.1.32:52882     ec2-52-86-108-169:https  TIME_WAIT
  TCP    192.168.1.32:52885     bidder:https           TIME_WAIT
  TCP    192.168.1.32:52886     724:https              ESTABLISHED
  TCP    192.168.1.32:52890     a23-45-115-75:https    TIME_WAIT
  TCP    192.168.1.32:52891     40.85.83.182:https     CLOSE_WAIT
  TCP    192.168.1.32:52892     a23-40-113-206:https   ESTABLISHED
  TCP    192.168.1.32:52893     ec2-35-178-2-28:https  ESTABLISHED
  TCP    192.168.1.32:52894     a23-212-157-206:https  ESTABLISHED
  TCP    192.168.1.32:52895     a23-212-157-206:https  ESTABLISHED
  TCP    192.168.1.32:52896     a23-212-157-206:https  ESTABLISHED
  TCP    192.168.1.32:52898     717:https              ESTABLISHED
  TCP    192.168.1.32:52899     717:https              ESTABLISHED
  TCP    192.168.1.32:52900     a95-100-95-154:https   TIME_WAIT
  TCP    192.168.1.32:52901     a95-100-95-154:https   TIME_WAIT
  TCP    192.168.1.32:52902     a88-221-113-74:https   ESTABLISHED
  TCP    192.168.1.32:52903     104.40.210.32:https    ESTABLISHED
  TCP    192.168.1.32:52904     a-0003:https           ESTABLISHED
  TCP    192.168.1.32:52905     51.104.144.132:https   ESTABLISHED
  TCP    192.168.1.32:52906     a-0001:https           ESTABLISHED
  TCP    192.168.1.32:52908     724:https              ESTABLISHED
  TCP    192.168.1.32:52909     ec2-54-154-162-165:https  ESTABLISHED
  TCP    192.168.1.32:52910     bidder:https           ESTABLISHED
  TCP    192.168.1.32:52911     213.19.162.21:https    ESTABLISHED
  TCP    192.168.1.32:52912     185.255.84.151:https   ESTABLISHED
  TCP    192.168.1.32:52913     213.19.162.21:https    ESTABLISHED
  TCP    192.168.1.32:52914     185.86.137.43:https    ESTABLISHED
  TCP    192.168.1.32:52915     a23-45-115-75:https    ESTABLISHED
  TCP    192.168.1.32:52916     23.102.47.40:https     ESTABLISHED
  TCP    192.168.1.32:52917     nyidt:https            ESTABLISHED
  TCP    192.168.1.32:52918     nyidt:https            ESTABLISHED
  TCP    192.168.1.32:52919     nyidt:https            ESTABLISHED
  TCP    192.168.1.32:52920     nyidt:https            ESTABLISHED
  TCP    192.168.1.32:52921     nyidt:https            SYN_SENT
  TCP    [::]:135               LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:445               LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:5357              LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:49664             LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:49665             LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:49666             LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:49667             LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:49669             LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::]:49684             LAPTOP-JHK9K6A7:0      LISTENING
  TCP    [::1]:49670            LAPTOP-JHK9K6A7:0      LISTENING
  UDP    0.0.0.0:67             *:*
  UDP    0.0.0.0:500            *:*
  UDP    0.0.0.0:3702           *:*
  UDP    0.0.0.0:3702           *:*
  UDP    0.0.0.0:4500           *:*
  UDP    0.0.0.0:5050           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5353           *:*
  UDP    0.0.0.0:5355           *:*
  UDP    0.0.0.0:45769          *:*
  UDP    0.0.0.0:50735          *:*
  UDP    0.0.0.0:52028          *:*
  UDP    0.0.0.0:53378          *:*
  UDP    0.0.0.0:54001          *:*
  UDP    0.0.0.0:57964          *:*
  UDP    0.0.0.0:61561          *:*
  UDP    127.0.0.1:1900         *:*
  UDP    127.0.0.1:10010        *:*
  UDP    127.0.0.1:10011        *:*
  UDP    127.0.0.1:60177        *:*
  UDP    127.0.0.1:62016        *:*
  UDP    127.0.0.1:63177        *:*
  UDP    192.168.1.32:137       *:*
  UDP    192.168.1.32:138       *:*
  UDP    192.168.1.32:1900      *:*
  UDP    192.168.1.32:2177      *:*
  UDP    192.168.1.32:5353      *:*
  UDP    192.168.1.32:62015     *:*
  UDP    [::]:500               *:*
  UDP    [::]:3702              *:*
  UDP    [::]:3702              *:*
  UDP    [::]:4500              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5353              *:*
  UDP    [::]:5355              *:*
  UDP    [::]:50736             *:*
  UDP    [::]:61562             *:*
  UDP    [::1]:1900             *:*
  UDP    [::1]:5353             *:*
  UDP    [::1]:62014            *:*
  UDP    [fe80::5564:4845:632e:1ad4%20]:1900  *:*
  UDP    [fe80::5564:4845:632e:1ad4%20]:2177  *:*
  UDP    [fe80::5564:4845:632e:1ad4%20]:62013  *:*







TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
 
 ```
 
* Port utilisé par le programme System :Processus inactif du système Ce processus est un thread unique qui fonctionne sur chaque processeur, sa seule fonction est d'occuper le temps processeur, lorsque le système ne fait tourner aucun autre thread

```

TCP    0.0.0.0:49684          0.0.0.0:0              LISTENING       892

```

* Port utilisé par le programme services.exe : Il s'agit du Service Control Manager (gestionnaire de contrôle des services) , responsable du démarrage, de l'arrêt et de l'interaction avec les services système .

```

TCP    192.168.1.32:54141     40.67.251.132:443      ESTABLISHED     12724

```

* Port utilisé par le programme msedge.exe : navigateur internet .

```



TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       948

```
* Port utilisé par le programme lsass.exe : (définition complète plus haut) s'agit du serveur local d'authentification de sécurité, il génère le processus responsable de l'authentification des utilisateurs par le service Winlogon .

```


TCP    192.168.1.32:55421     8.241.59.254:80        ESTABLISHED     6840
 ```
 
* Port utilisé par le programme svchost.exe : Fonctionne en tant qu'hôte pour d'autres processus tournant à partir de Dlls, ces processus fonctionnent en tant que services. Un processus svchost.exe peut gérer plusieurs autres processus. 


 # II. Scripting



      
 # III. Gestion des softs
 * Par rapport a un téléchargement direct sur internet , le gestionnaire de paquets va permettre de télécharger un fichier plus rapidement et plus sécurisée car il va prendre le fichier correspondant qui est le plus fiable , de plus le fichier ne sera pas altérée .

* Il y a l'hébergeur du site , les personnes qui mettent en place les téléchargement , potentiellement des hackers et enfin les personnes qui souhaitent télécharger un fichier .


* Il est possible qu'une personne altère au fichier entre l'hébergeur du site et la personne qui souhaite télécharger le fichier venant du site .

```
PS C:\Users\lucas> choco list --local-only

Chocolatey v0.10.15
chocolatey 0.10.15
chocolatey-core.extension 1.3.5.1
Firefox 82.0.2
3 packages installed.

PS C:\Users\lucas> choco list --local-only --detailed
[...]
chocolatey-core.extension 1.3.5.1
[...]
 Chocolatey Package Source: https://github.com/chocolatey/chocolatey-coreteampackages/tree/master/extensions/chocolatey-core.extension
 [...]
 Firefox 82.0.2
 [...]
Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/firefox

```


  
# IV. Machine Virtuelle 

* Pour commencer , nous allons dans le panneau de configuration => programme => désinstaller un programme => activer ou désactiver des fonctionnalités windows puis cocher toutes les cases du dossiers Serveur de partage de fichiers SMB 1.0/CIFS

* Ensuite nous devons créer un dossier (je l'ai nommé share) sur le bureau

* Ensuite on fait clique droit sur le dossier et on fait propriété => partage => partage avancé puis dans Autorisations :

![](https://i.imgur.com/Pp8Afm7.png)


* On clique sur "Tout le monde" et on coche toutes les cases (Contrôle total ; Modifier ; Lecture) en autoriser. On Applique puis ok



* Ensuite on lance la vm depuis powershell et on crée un nouveau SMB avec la commande suivante : New-SmbShare -Name "share" -Path "C:\Users\lucas\Desktop\share"

* Ensuite on se connecte en root . On installe le paquet qui permet d'utiliser les partages samba : yum install -y cifs-utils

* créer un dossier qui va permettre d'accéder au partage : mkdir /opt/partage


* On monte le partage dans la Vm : mount -t cifs -o username=(mon utilisateur),password=(mon mot de passe) //l'ip de mon ordi(pour moi c'est l'ip de ma carte wi-fi)/share /opt/partage

 
* Maintenant nous avons accès au dossier , pour vérifier que cela marche bien , j'ai effectué quelques manipulations (créer, supprimer et éditer) :



![image-création-fichier](https://i.imgur.com/ZssTj4q.png)

![image-suppression-fichier](https://i.imgur.com/KZREAug.png)

![image-edit-text.txt](https://i.imgur.com/DvXIwwT.png)



